/*
 * Copyright 2019 Johannes Strelka-Petz, johannes_at_strelka.at
 *
 */

/*
 * This file is part of oskar_concertina_case.
 *
 */
 
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

// kailh_choc_PG1350
kailh_choc_PG1350_height=9.9;
echo(kailh_choc_PG1350_height=9.9);

// keycap
        keycap_h=3.2;
        keycap_x=17.5;
        keycap_y=16.5;
        
// kailh_choc_PG1350 raster
        kailh_choc_PG1350_dx=keycap_x+1;
        kailh_choc_PG1350_dy=keycap_y+1;

tolerance1=1;
tolerance2=0.5;


// keypad
keypad_size_x=kailh_choc_PG1350_dx*2;
echo(keypad_size_x=kailh_choc_PG1350_dx*2);
keypad_size_y=kailh_choc_PG1350_dy*4;
echo(keypad_size_y=kailh_choc_PG1350_dy*4);
keypad_size_z=kailh_choc_PG1350_height;
echo(keypad_size_z=kailh_choc_PG1350_height);

// lipo      
lipo_length=40;
lipo_width=25;
lipo_height=6.5;
lipo_protector_length=lipo_length-4.5;

//pin
pin_w=0.64;
pin_h=11.54;
pin_low=3;
pin_high=6;
pin_block_width=2.5;
pin_rm=2.54;

// lolind32
lolind32_length=57.5;
echo(lolind32_length=57.5);
lolind32_width=25.5;
echo(lolind32_width=25.5);
lolind32_pcb_height=1;
lolind32_height=lolind32_pcb_height+6.5+pin_block_width;
echo(lolind32_height=lolind32_pcb_height+6.5);
lolind32_tolerance=0.2;
lolind32_usb_h=2.5;
lolind32_usb_w=7.5;

// onoff
onoff_size_length=13.8;
echo(onoff_size_length=13.8);
onoff_size_width=6.5;
onoff_size_height=8.5;
onoff_size_lever_length=9;
echo(onoff_size_lever_length=9);
onoff_size_lever_z=4;
onoff_lever_offset_z=5.5-2;

//magnet
magnet_d=18;
magnet_h=4;

// screw
screw_l=12;
screw_d=3.5;
screw_dk=6;
screw_k=1.86;
screw_h=screw_l-screw_k;
screw_bottom_d=5;
// thread inserts
threadin_d=5.4;
threadin_h=7;     

//wall
wall_bottom_width=1;
wall=2;

// connector_jst_ph
con_length=8.6;
con_width=6.9;
con_height=6.5;

// pcb
pcb_width=1.6;

bottom_to_lolind32=1;
screw_savespace=2;
magnetl_x=-magnet_d/2-screw_savespace-tolerance1*2;
magnetr_x=keypad_size_x+magnet_d/2+screw_savespace+tolerance1*2;
magnet_z=magnet_h-pcb_width-lolind32_height-wall_bottom_width-bottom_to_lolind32;
screw_bottom_height=-pcb_width-(magnet_z-magnet_h+wall_bottom_width);
echo(screw_bottom_height=screw_bottom_height);
screw_front_h=keypad_size_z-wall;		 
pcb_cone_distance=1;

lolind32_offset_x=keypad_size_x/2-lolind32_width+pin_w*2-1;
onoffpcb_y=-onoff_size_width-1+keypad_size_y-onoff_size_lever_length*3/4+2;
echo(onoffpcb_y-keypad_size_y);

// pcb mount screws
screw_left_reality_offset_x=0.5;
screw_left_reality_offset_y=1;
screw_right_reality_offset_x=1;
screw_right_reality_offset_y=1;

screw_z=(keypad_size_z-wall)/2;

screw_bottom_left_x=lolind32_offset_x-(screw_bottom_d+4)/2-1-screw_left_reality_offset_x;
echo(screw_bottom_left_x=screw_bottom_left_x);
screw_bottom_left_y=(screw_bottom_d+4)/2-wall/2;
echo(screw_bottom_left_y=screw_bottom_left_y);

screw_bottom_right_x=magnetr_x+magnet_d/2-(screw_bottom_d+4)/2;
screw_bottom_right_y=screw_bottom_left_y;
tolerance_front_back=0.2;
screw_bottom_right_height=keypad_size_z-wall+pcb_width-tolerance_front_back;

screw_top_right_x=screw_bottom_left_x+53.72+screw_right_reality_offset_x;
echo(screw_top_right_x=screw_top_right_x);
screw_top_right_y=screw_bottom_left_y+62+screw_right_reality_offset_y;
echo(screw_top_right_y=screw_top_right_y);

screw_top_left_x=screw_bottom_left_x+0.08;
echo(screw_top_left_x=screw_top_left_x);
screw_top_left_y=screw_bottom_left_y+49.75+screw_left_reality_offset_y;
echo(screw_top_left_y=screw_top_left_y);

screw_back_bottom_top_h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width);
screw_back_bottom_back_h=-(magnet_z-magnet_h+wall_bottom_width)-(pcb_width+pin_block_width+1+lolind32_usb_h*0)-tolerance2;

// cut out
//usb_cutout_left_x=-tolerance1-(-lolind32_offset_x-2)-tolerance2+tolerance1;
usb_cutout_left_x=lolind32_offset_x+2-tolerance2;

front_outer_wall_height=-magnet_z+magnet_h+wall_bottom_width+keypad_size_z-wall-wall-(onoff_lever_offset_z+onoff_size_lever_z);
front_outer_height=front_outer_wall_height+1.5;
echo(front_outer_height=front_outer_wall_height+1.5);

//front_outer_wall_height=(keypad_size_z-wall)-(-pcb_width-pin_block_width-1);

// case
cornerradius=5;
case_back_wall_height=-magnet_z+magnet_h-wall_bottom_width-pcb_width-(pin_block_width+1+lolind32_usb_h*0+tolerance2);

//arrangement="pcb_export";
arrangement="stacked_long";
//arrangement="flat";
//arrangement="flat_wm";

//develop
//facetcount=8;
// finish
facetcount=24;

$fn=facetcount;

module kailh_choc_PG1350(){
    pg1350_l=13.8;
    pg1350_h=5;    
    pg1350_ol=15;
    pg1350_olh=0.8;
    pg1350_cd=3.2;
    pg1350_cd2=1.9;
    pg1350_ch=2.65;
    translate([0,0,pg1350_h/2]){
        color("brown"){
            cube([pg1350_l,pg1350_l,pg1350_h],true);
            cube([pg1350_ol,pg1350_ol,pg1350_olh],true);
        }
    
//  bolts  
        translate([0,0,-pg1350_ch-pg1350_h/2]){
            cylinder(pg1350_ch, d = pg1350_cd,false);
            translate([5.5,0,0]){
                cylinder(pg1350_ch, d = pg1350_cd2,false);
                }
            translate([-5.5,0,0]){
                cylinder(pg1350_ch, d = pg1350_cd2,false);
                }
            }
//  keycap
        translate([0,0,keycap_h/2+4.2]){
            color("grey"){
                cube([keycap_x,keycap_y,keycap_h],true);
            }
            }
        }
//// keycap size
//        translate([0,0,kailh_choc_PG1350_height/2]){
//            #cube([keycap_x,keycap_y,4.2+0.9+1.6+3.2],true);
//        }
    }
//kailh_choc_PG1350();
    
module keypad(){
    translate([kailh_choc_PG1350_dx/2,kailh_choc_PG1350_dy/2,0]){
        for(row=[0:1]){
            for(col=[0:3]){
                translate([row*kailh_choc_PG1350_dx,col*kailh_choc_PG1350_dy,0]){
                    kailh_choc_PG1350();
                    }
                }
            }
        }
////        keypadsize
//        #cube([kailh_choc_PG1350_dx*2,4*kailh_choc_PG1350_dy,kailh_choc_PG1350_height]);
    }
if (arrangement=="stacked_long"){
     *keypad();
     }
    
module lipo() {
    color("red"){
	cube([lipo_length,lipo_width,lipo_height]);
    }
}
*lipo();

module lipo_protector_waves(){
     distance=0.6;
     p_width=0.7;
     voxel_size=1.5;
     
     /* intersection(){ */
     /* 	  translate([0,0,-2]){ */
     /* 	  cube([ */
     /* 		     lipo_length+voxel_size, */
     /* 		     lipo_width+distance+p_width+voxel_size, */
     /* 		     p_width+4 */
     /* 		     ]); */
     /* 	  } */
	  for(x=[0:voxel_size:lipo_protector_length+voxel_size]){
	       for(y=[0.:voxel_size:lipo_width+distance+p_width+voxel_size]){
		    translate([x,y,0.8*cos(x*360*2/lipo_protector_length+0.01)*sin(y*360*2/(lipo_width+distance+p_width+0.01))]){

		    	 cube([
		    		   voxel_size*1.1,
		    		   voxel_size*1.1,
		    		   voxel_size]);
		    }
		    /* translate([x,y,cos(x*360*2/lipo_length+0.01)*sin(y*360*2/(lipo_width+distance+p_width+0.01))]){ */
		    /* 	 sphere(d=voxel_size*1.5); */
		    /* } */
		    //echo(y1=cos(360*y/(lipo_width+distance+p_width)));
	       }
	       //echo(x1=sin(360*x/lipo_length));		    
	  }
}
/* } */
*lipo_protector_waves();

module lipo_protector(){
     distance=0.6;
     p_width=0.8;
     voxel_size=1.5;
     color("olive"){
	  translate([0,-distance-p_width,lipo_height+distance+p_width*0]){
	       cube([lipo_protector_length,lipo_width,p_width]);		    
	       }
	  translate([0,-distance-p_width,lipo_height+distance+p_width-1]){
	       *lipo_protector_waves();
	  }

	  translate([0,-distance-p_width,0]){
	       cube([
			 lipo_protector_length,
			 p_width,
			 lipo_height+distance+p_width
			 ]);
	  }
	  //}
     }
}
*lipo_protector();

  
module pin_head(){
//    translate([0,0,-pin_h/2+pin_low]){
    translate([0,0,-pin_h/2+pin_high]){    
        cube([pin_w,pin_w,pin_h],center=true);
        }
    translate([0,0,-pin_block_width/2]){
        color("black"){
            cube([pin_block_width,pin_rm,pin_block_width],center=true);
            }
        }
    }
    
    
module lolind32(usboverlap=0, tolerance=0){
    translate([0,0,pin_block_width]){
    color("green"){
//	difference(){ //preview error
    %union(){
		translate([-tolerance/2,-tolerance/2,-tolerance/2,]){
            // pcb
            cube([lolind32_length+tolerance,lolind32_width+tolerance,lolind32_pcb_height+tolerance]);
            }
        // drill holes
	    translate([55.5,2,0.5]){
            cylinder(h=2,d=2,center=true,$fn=facetcount);
            }
	    translate([55.5,23.5,0.5]){
            cylinder(h=2,d=2,center=true,$fn=facetcount);
            }
        translate([8,2,0.5]){
            cylinder(h=2,d=2,center=true,$fn=facetcount);
            }     
        }
        }       
    translate([33,4.5,1]){        
        // esp32
        color("magenta"){
            cube([18,16,3]);
            }
        }
    translate([33+18,4.5,1]){
        // antenna
        color("blue"){
            cube([lolind32_length-33-18,16,1.2]);
            }
        }
    // usb connector
    translate([-0.5-usboverlap,2-tolerance/2,1-tolerance/2]){
        color("white"){
            cube([5.5+usboverlap,lolind32_usb_w+tolerance,lolind32_usb_h+tolerance]);
        }
    }
    // battery connector
    translate([0.5-tolerance,2+7.5+6.5-tolerance/2,1-tolerance/2]){
        color("white"){
            cube([6,8+tolerance,6.5+tolerance]);
            }
        }
//    // lolind32 size
//    %cube([lolind32_length, lolind32_width,lolind32_height]);

    for(i=[6:15]){
        translate([12,1+pin_w/2,0]){
            translate([i*pin_rm,0,0]){
                pin_head();
                }
            }
        }
    for(i=[0:15]){            
        translate([12,lolind32_width-1-pin_w/2,0]){
            translate([i*pin_rm,0,0]){
                pin_head();
                }
            }

        }   
    }
}

module onoff(){
    // P040042
    color("orange"){
        cube([onoff_size_length,onoff_size_width,onoff_size_height]);
            translate([(onoff_size_length-4-5)/2,-onoff_size_lever_length,onoff_lever_offset_z]){
                cube([5,onoff_size_lever_length,onoff_size_lever_z]);
            }
        }   
//    // P040042 size
//    translate([0,-9,0]){
//        %cube([13.8,6.5+onoff_size_lever_length,8.5]);
//        }
    }

module connector_jst_ph(){
    cube([con_width,con_length,con_height]);
    }


module magnet(md=magnet_d,tolerance_m=0){
     translate([0,0,-(magnet_h+tolerance_m)/2]){
	  cylinder(h=magnet_h+tolerance_m,d=md,center=true,$fn=facetcount);
//        screw
	  translate([0,0,screw_h/2+magnet_h/2]){
	       cylinder(h=screw_h,d=screw_d,center=true,$fn=facetcount);
	  }
	  translate([0,0,threadin_h/2+magnet_h/2]){
	       cylinder(h=threadin_h,d=threadin_d,center=true,$fn=facetcount);
	  }

     }
}



module pcb(){
    
     color("green"){        
	  union(){
	       translate([lolind32_offset_x,0,0]){
		    translate([0,0,-pcb_width]){
			 cube([keypad_size_x-lolind32_offset_x+wall,
			       keypad_size_y,
			       pcb_width]);
                    }
	       }           
	       translate([-onoff_size_length-3,onoffpcb_y,-pcb_width]){
		    cube([onoff_size_length+2,keypad_size_y-onoffpcb_y,pcb_width]);
	       }
//            battery connector
	       translate([keypad_size_x-1,
//                keypad_size_y-con_length-2,
			  keypad_size_y/4*3-con_length-2,
			  -pcb_width]){
		    cube([con_width+1,keypad_size_y-(screw_bottom_d+4)-(keypad_size_y/4*3-con_length-2),pcb_width]);
	       }
//            pcb screw back left
	       translate([lolind32_offset_x-(screw_bottom_d+4)/2,
			  (screw_bottom_d+4)/2-wall/2+0.5,
			  -pcb_width/2]){
		    cube([screw_bottom_d+4+2,screw_bottom_d+4-1,pcb_width],center=true);
	       }                
//            pcb screw front left
	       translate([lolind32_offset_x+(-onoff_size_length-3-lolind32_offset_x)/2,
			  onoffpcb_y-(screw_bottom_d+4)/2,
			  -pcb_width/2]){
		    cube([onoff_size_length+3+lolind32_offset_x,screw_bottom_d+4,pcb_width],center=true);
	       }                
//            pcb screw right
	       translate([keypad_size_x+(screw_bottom_d+4)/2,
			  keypad_size_y-(screw_bottom_d+4)/2,
			  -pcb_width/2]){
		    cube([screw_bottom_d+4,screw_bottom_d+4,pcb_width],center=true);
	       }                
	  }
     }
}

module intern(){
    translate([lipo_width+lolind32_width-4,0,-lipo_height-pcb_width-3]){
        rotate([0,0,90]){
            lipo();
	    lipo_protector();
            }
        }

    translate([lolind32_offset_x,0,-pcb_width]){
        rotate([180,0,90]){
            lolind32(usboverlap=2);
            }
        }

    translate([-onoff_size_length+0*keypad_size_x-2,keypad_size_y-onoff_size_lever_length*3/4+2,-pcb_width]){
        rotate([0,180,180]){
            onoff();
            }
        }

    translate([magnetl_x,
        keypad_size_y/4,
        magnet_z]){
         magnet(magnet_d);
        }
        
    translate([
        magnetr_x,
//        keypad_size_x+screw_d/2-lolind32_offset_x+screw_savespace,
//        keypad_size_x+screw_d/2+screw_savespace,
        keypad_size_y/4*3,
        magnet_z]){
         magnet(magnet_d);
        }
    translate([keypad_size_x-1,keypad_size_y/4*3-con_length-1,-pcb_width-con_height]){
        connector_jst_ph();
        }
    pcb();
}
*intern();

module case_outer(crad=cornerradius){
     color("green"){
	  translate([magnetl_x-magnet_d/2-wall-tolerance1,
		     -wall-tolerance1,
		     magnet_z-magnet_h-wall_bottom_width*0]){
	       translate([crad,crad,crad]){
		    minkowski(){
			 cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2-crad*2
			       +0.042,
			       keypad_size_y+2*wall+tolerance1*3-crad*2
			       +0.042,
			       front_outer_wall_height+wall+tolerance2
			       +(case_back_wall_height+wall_bottom_width)
			       -crad*2
			       +0.042]);
			 sphere(r=crad);
		    }
	       }
	  }
     }
}
*case_outer(crad=cornerradius);

module case_inner(){
     color("blue"){
	  translate([magnetl_x-magnet_d/2-wall-tolerance1+wall,
		     -wall-tolerance1+wall,
		     magnet_z-magnet_h+wall_bottom_width]){
	       translate([cornerradius,cornerradius,cornerradius]){
		    //difference(){
			 minkowski(){
			      cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2-cornerradius*2-wall*2,
				    keypad_size_y+2*wall+tolerance1*3-cornerradius*2-wall*2,
				    front_outer_wall_height+wall+tolerance2
				    +(case_back_wall_height+wall_bottom_width)
				    -cornerradius*2
				    -wall-wall_bottom_width]);
			      sphere(r=cornerradius);
			 }
			 /* // -x */
			 /* translate([-(magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2-cornerradius*2-wall*2)/2,0,0]){			  */
			 /*      cube([(magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2-cornerradius*2-wall*2)*2, */
			 /* 	    (keypad_size_y+2*wall+tolerance1*3-cornerradius*2-wall*2), */
			 /* 	    (front_outer_wall_height+wall+tolerance2+(case_back_wall_height+wall_bottom_width)-cornerradius*2-wall-wall_bottom_width) */
			 /* 		 ]); */
			 /* } */
			 /* // -y */
			 /* translate([0,-(keypad_size_y+2*wall+tolerance1*3-cornerradius*2-wall*2)/2,0]){			  */
			 /*      cube([(magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2-cornerradius*2-wall*2), */
			 /* 	    (keypad_size_y+2*wall+tolerance1*3-cornerradius*2-wall*2)*2, */
			 /* 	    (front_outer_wall_height+wall+tolerance2+(case_back_wall_height+wall_bottom_width)-cornerradius*2-wall-wall_bottom_width) */
			 /* 		 ]); */
			 /* } */
			 /* // -z */
			 /* translate([0,0,-(front_outer_wall_height+wall+tolerance2+(case_back_wall_height+wall_bottom_width)-cornerradius*2-wall-wall_bottom_width)/2]){			  */
			 /*      cube([(magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2-cornerradius*2-wall*2), */
			 /* 	    (keypad_size_y+2*wall+tolerance1*3-cornerradius*2-wall*2), */
			 /* 	    (front_outer_wall_height+wall+tolerance2+(case_back_wall_height+wall_bottom_width)-cornerradius*2-wall-wall_bottom_width)*2 */
			 /* 		 ]); */
			 /* } */
		    //}
	       }
	  }
	  // front
	  // keypad keys hole
	  translate([-tolerance1,
		     -tolerance1,
		     keypad_size_z-wall-0.5-cornerradius]){
	       cube([keypad_size_x+tolerance1*2,keypad_size_y+tolerance1*3,wall+1+cornerradius]);
	  }
	  // back
	  // magnetl
          translate([magnetl_x,
		     keypad_size_y/4,
		     magnet_z]){
	       magnet(magnet_d+tolerance1*1.5+0.1);
	  }
	  translate([magnetl_x,
	  	     keypad_size_y/4,
	  	     magnet_z-magnet_h+wall_bottom_width/2]){
	       cylinder(h=wall_bottom_width+0.1,d2=magnet_d+tolerance1*1.5+0.1,d1=magnet_d+tolerance1*1.5+0.1+wall_bottom_width+0.1,center=true);
	  }
	  // magnetr
	  translate([
			 magnetr_x,
			 keypad_size_y/4*3,
			 magnet_z]){
	       magnet(magnet_d+tolerance1*1.5+0.1);
	  }
	  
	  //magnetr_x=keypad_size_x+magnet_d/2+screw_savespace;
	  translate([
	  		 magnetr_x,
	  		 keypad_size_y/4*3,
	  		 magnet_z-magnet_h+wall_bottom_width/2]){
	       cylinder(h=wall_bottom_width+0.1,d2=magnet_d+tolerance1*1.5+0.1,d1=magnet_d+tolerance1*1.5+0.1+wall_bottom_width+0.1,center=true);
	  }

	  //            screw right top (pcb: right top)
	  translate([
			 screw_top_right_x, //keypad_size_x+(screw_bottom_d+4)/2,
			 screw_top_right_y, //keypad_size_y-(screw_bottom_d+4)/2,
			 -screw_bottom_height/2-pcb_width]){
	       cylinder(h=screw_bottom_height,d=(screw_bottom_d+4),center=true);
	  }
	  translate([
	  		 screw_top_right_x,
	  		 screw_top_right_y,
	  		 magnet_z-magnet_h+wall_bottom_width/2]){
	       cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);
	  }

	  //  	       screw back left bottom (pcb: left bottom)
	  translate([
			 screw_bottom_left_x,
			 screw_bottom_left_y,
			 (-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){			      
	       cylinder(h=screw_back_bottom_back_h*0+screw_back_bottom_top_h,d=(screw_bottom_d+4-tolerance1*0),center=true);
	  }
	  translate([
	  		 screw_bottom_left_x,
	  		 screw_bottom_left_y,
	  		 magnet_z-magnet_h+wall_bottom_width/2]){
	       cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);
	  }
	  //            screw back left bottom (pcb: left top)
	  translate([
			 screw_top_left_x, //lolind32_offset_x-(screw_bottom_d+4)/2-1-screw_left_reality_offset_x,
			 screw_top_left_y, //onoffpcb_y-(screw_bottom_d+4)/2,
			 (-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){
	       cylinder(h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width),d=(screw_bottom_d+4),center=true);
	  }
	  translate([
	  		 screw_top_left_x,
	  		 screw_top_left_y,
	  		 magnet_z-magnet_h+wall_bottom_width/2]){
	       cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);
	  }
	  //            screw back right bottom (pcb: left top)
	  translate([screw_bottom_right_x,
		      screw_bottom_right_y,
		      (-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){
	       cylinder(h=screw_back_bottom_back_h*0+screw_back_bottom_top_h,d=(screw_bottom_d+4-tolerance1*0),center=true);
	       //cylinder(h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width),d=(screw_bottom_d+4),center=true);			 		     
	  }
	  translate([
	  		 screw_bottom_right_x,
	  		 screw_bottom_right_y,
	  		 magnet_z-magnet_h+wall_bottom_width/2]){
	       cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);
	  }
	  // top on off switch
	  lever_dist_x=(onoff_size_length+2)/2-(onoff_size_length-4-5)/2;
	  translate([
			 -(onoff_size_length+2)+(onoff_size_length-4-5)/2+tolerance2+-tolerance1,
			 keypad_size_y+tolerance1*2
			 -1,
			 magnet_z-magnet_h+wall_bottom_width+onoff_lever_offset_z-tolerance2]){
	       cube([
			 (-lever_dist_x+tolerance2)-(-(onoff_size_length+2)+(onoff_size_length-4-5)/2+tolerance2+-tolerance1),
			 wall+2,
			 onoff_size_lever_z]);
	  }	    
          // bottom back usb
	  translate([(magnetl_x-magnet_d/2-wall-tolerance1)
		     +(usb_cutout_left_x-(magnetl_x-magnet_d/2-wall-tolerance1)-tolerance1),
		     -wall-tolerance1
		     -1,
		     -pcb_width-pin_block_width-1-lolind32_usb_h-tolerance2]){       
	       cube([
			 /* ((lolind32_offset_x+2+lolind32_usb_w)+tolerance2) */
			 /* -((magnetl_x-magnet_d/2-wall-tolerance1) */
			 /*   +(usb_cutout_left_x-(magnetl_x-magnet_d/2-wall-tolerance1)-tolerance1)), */
			 ((lolind32_offset_x+2+lolind32_usb_w)+tolerance2)
			 -((magnetl_x-magnet_d/2-wall-tolerance1)
			   +(usb_cutout_left_x-(magnetl_x-magnet_d/2-wall-tolerance1)-tolerance1)),
			 wall+2,
			 lolind32_usb_h]);
	  }
	  
     }
}
*case_inner();

module front(walls=true){
     color("Cyan"){
	  union(){
//            front
	       difference(){
		    if(walls){
			 translate([magnetl_x-magnet_d/2-wall-tolerance1,
				    -wall-tolerance1,
				    keypad_size_z-wall]){
			      cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2,keypad_size_y+2*wall+tolerance1*3,wall]);
			 }
		    }
		    if(walls){
			 translate([-tolerance1,
				    -tolerance1,
				    keypad_size_z-wall-0.5]){
			      cube([keypad_size_x+tolerance1*2,keypad_size_y+tolerance1*3,wall+1]);
			 }
		    }
	       }
	       if(walls){		    
//            top
//            top front	    
		    translate([magnetl_x-magnet_d/2-wall-tolerance1,
			       keypad_size_y+tolerance1*2,
			       magnet_z-magnet_h+wall_bottom_width+onoff_lever_offset_z+onoff_size_lever_z+tolerance2]){
			 cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2,
				wall,
				front_outer_wall_height]);
			 //
		    }
//            bottom front
		    translate([magnetl_x-magnet_d/2-wall-tolerance1,
			       -wall-tolerance1,
			       -pcb_width-pin_block_width-1+tolerance2]){
			 cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2,
				wall,
				front_outer_wall_height]);
			 // -magnet_z+magnet_h+wall_bottom_width+keypad_size_z-wall-wall]);
		    }
//            left
		    translate([magnetl_x-magnet_d/2-wall-tolerance1,
			       -tolerance1,
			       -pcb_width-pin_block_width-1+tolerance2]){
			 cube([wall,
				keypad_size_y+tolerance1*3,
				front_outer_wall_height]);
		    }
//            right
		    translate([magnetr_x+magnet_d/2+tolerance1,
			       -tolerance1,
			       -pcb_width-pin_block_width-1+tolerance2]){
			 cube([wall,
				keypad_size_y+tolerance1*3,
				front_outer_wall_height]);
		    }
	       }
//            keypad left
	       difference(){
		    translate([-wall-tolerance1,
			       -tolerance1*2,
			       tolerance1]){
			 cube([wall,
			       keypad_size_y+tolerance1*4,
			       keypad_size_z-wall]);
		    }
//	       resistor space
		    translate([-wall-tolerance1,onoffpcb_y-(screw_d+4)+1-pin_rm*5,0]){
			 cube([wall+tolerance1,pin_rm*5,3],center=false);
		    }
//                  onoff switch space
		    onoff_switch_space_offsetx=2;
		    onoff_switch_space_offsety=1;
		    onoff_switch_space_offsetz=5;	       
		    translate([-onoff_size_length+0*keypad_size_x-2+onoff_switch_space_offsetx,
			       keypad_size_y-onoff_size_lever_length*3/4+2+onoff_switch_space_offsety*4,
			       -pcb_width+onoff_switch_space_offsetz]){
			 rotate([0,180,180]){
			      onoff();
			 }
		    }
		    translate([-onoff_size_length+0*keypad_size_x-2+onoff_switch_space_offsetx,
			       keypad_size_y-onoff_size_lever_length*3/4+2-onoff_switch_space_offsety,
			       -pcb_width+onoff_switch_space_offsetz]){
			 rotate([0,180,180]){
			      onoff();
			 }
		    }
	       }
//            magnet screw left
	       difference(){
		    translate([magnetl_x,
			       keypad_size_y/4,
			       (keypad_size_z-wall+magnet_z)/2]){
			 cylinder(h=keypad_size_z-wall-magnet_z,d=screw_d+4,center=true);
		    }
		    translate([magnetl_x,
			       keypad_size_y/4,
			       magnet_z]){
			 magnet(magnet_d);
		    }
	       }
//            magnet screw right
	       difference(){
		    translate([magnetr_x,
			       keypad_size_y*3/4,
			       (keypad_size_z-wall+magnet_z)/2]){
			 cylinder(h=keypad_size_z-wall-magnet_z,d=screw_d+4,center=true);
		    }
		    translate([
				   magnetr_x,
				   keypad_size_y/4*3,
				   magnet_z]){
			 magnet(magnet_d);
		    }
	       }
//            screw front left (pcb: left top)
	       difference(){
		    union(){
			 translate([screw_top_left_x,
				    screw_top_left_y,
				    (keypad_size_z-wall)/2]){
			      cylinder(h=screw_front_h,d=screw_bottom_d+4,center=true);
			 }
			 translate([lolind32_offset_x-(screw_bottom_d+4)/2-1,onoffpcb_y-(screw_d+4)+1,tolerance1]){
			      cube([-lolind32_offset_x+(screw_d+4)/2,(screw_d+4)-1,keypad_size_z-wall], center=false);
			 }
		    }
		    translate([screw_top_left_x,
			       screw_top_left_y,
			       screw_z]){
			 cylinder(h=screw_front_h+1,d=screw_d,center=true);
			 translate([0,0,(threadin_h-screw_front_h-1)/2]){cylinder(h=threadin_h+1,d=threadin_d,center=true);}
		    }
	       }
//            screw back left (pcb: left bottom)
	       difference(){
		    union(){
			 translate([screw_bottom_left_x, //lolind32_offset_x-(screw_bottom_d+4)/2-1-screw_left_reality_offset_x,
				    screw_bottom_left_y, //(screw_bottom_d+4)/2-wall/2,
				    (keypad_size_z-wall)/2]){
			      cylinder(h=keypad_size_z-wall,d=screw_bottom_d+4,center=true);
			      // supportstructure for screw back left
			      translate([0,-(screw_bottom_d+4)/4-tolerance1,0]){
				   cube([(screw_bottom_d+4),(screw_bottom_d+4)/2+tolerance1*2,keypad_size_z-wall],center=true);
			      }
			 }
			 // supportstructure for magnetscrew
			 translate([magnetl_x-0.4-screw_left_reality_offset_x*0,
				    keypad_size_y/4-(screw_d+4)+0.6,
				    (keypad_size_z-wall)/2]){
			      rotate([0,0,2]){
				   cube([screw_bottom_d*0+4-tolerance2+2,screw_bottom_d+4+1,keypad_size_z-wall],center=true);
			      }
			 }
		    }
		    translate([screw_bottom_left_x,
			       screw_bottom_left_y,
			       screw_z]){
			 cylinder(h=screw_front_h+1,d=screw_d,center=true);
			 translate([0,0,(threadin_h-screw_front_h-1)/2]){cylinder(h=threadin_h+1,d=threadin_d,center=true);}
		    }
	       }

//            translate([lolind32_offset_x-screw_dk/2-(screw_d+4)/2,0,0]){
//                #cube([screw_d+4,(screw_d+4)/2,keypad_size_z-wall], center=false);
//                }
	    
//          keypad right and screw right
	       difference(){
		    union(){
			 //            keypad right
			 translate([keypad_size_x+tolerance1,
				    -tolerance1*2,
				    tolerance1]){
			      cube([wall,
				    keypad_size_y+tolerance1*4,
				    keypad_size_z-wall]);
			 }
			 //            screw right
			 translate([keypad_size_x+(screw_bottom_d+4)/2,
				    keypad_size_y-(screw_bottom_d+4)/2,
				    (keypad_size_z-wall)/2]){
			      *cylinder(h=keypad_size_z-wall,d=screw_bottom_d+4,center=true);
			 }
			 translate([keypad_size_x+(screw_bottom_d+4)/2+(tolerance1)/2,
				    keypad_size_y-(screw_bottom_d+4)/2+tolerance1,
				    (keypad_size_z-wall)/2]){
			      cube([screw_bottom_d+4-tolerance2-(tolerance1/2),
				     screw_bottom_d+4+tolerance1*4,
				     keypad_size_z-wall],center=true);
			 }
			 // supportstructure for magnetscrew
			 translate([magnetr_x-(screw_d+4)/2,
				    keypad_size_y*3/4+(screw_d+4)/2+2,
				    (keypad_size_z-wall)/2]){
			      rotate([0,0,35]){
				   cube([screw_bottom_d*0+4-tolerance2,screw_bottom_d+4-1,keypad_size_z-wall],center=true);
			      }
			 }			 
		    }
		    // screw right (pcb: right top)
		    translate([screw_top_right_x,
			       screw_top_right_y,
			       screw_z]){
			 cylinder(h=screw_front_h+1,d=screw_d,center=true);
			 translate([0,0,(threadin_h-screw_front_h-1)/2]){cylinder(h=threadin_h+1,d=threadin_d,center=true);}
		    }
		    // connector_jst_ph space
		    connector_jst_ph_offset_z=5;
		    connector_jst_ph_space_length=con_height*3/4;
		    translate([keypad_size_x-1,keypad_size_y/4*3-connector_jst_ph_space_length-1,-pcb_width-con_height+connector_jst_ph_offset_z]){
			 cube([con_width,connector_jst_ph_space_length,con_height]);
		    }
	       }
	       // screw bottom right front
	       difference(){
		    union(){
			 translate([screw_bottom_right_x,
				    screw_bottom_right_y,
				    (keypad_size_z-wall)-(screw_bottom_right_height)/2]){
			      cylinder(h=screw_bottom_right_height,d=screw_bottom_d+4,center=true);
			      translate([0,-(screw_bottom_d+4)/4-tolerance1,0]){
				   cube([(screw_bottom_d+4),(screw_bottom_d+4)/2+tolerance1*2,screw_bottom_right_height],center=true);
			      }
			 }
			 translate([
					screw_bottom_right_x+((screw_bottom_d+4)/2+tolerance1)/2,
					screw_bottom_left_y,
					(keypad_size_z-wall)-(screw_bottom_right_height)/2]){					     
			      cube([(screw_bottom_d+4)/2+tolerance1,screw_bottom_d+4,screw_bottom_right_height],center=true);
			 }		    
		    }
		    translate([screw_bottom_right_x,
			       screw_bottom_right_y,
			       (keypad_size_z-wall)-(screw_bottom_right_height)/2]){
			 cylinder(h=screw_front_h+1,d=screw_d,center=true);
			 translate([0,0,(threadin_h-screw_front_h-1)/2]){cylinder(h=threadin_h+1,d=threadin_d,center=true);}
		    }
	       }	    
	       
	  }
     }
}

module back(walls=true){
     color("yellow"){
	  union(){
//            back
	       if(walls){	       
	       difference(){
		    translate([magnetl_x-magnet_d/2-wall-tolerance1,
			       -wall-tolerance1,
			       magnet_z-magnet_h-wall_bottom_width*0]){
			 cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2,keypad_size_y+2*wall+tolerance1*3,wall_bottom_width]);
		    }
                    // magnet left
		    translate([magnetl_x,
			       keypad_size_y/4,
			       magnet_z]){
			 magnet(magnet_d+tolerance1*1.5+0.1);
		    }
		    translate([magnetl_x,
			       keypad_size_y/4,
			       magnet_z-magnet_h+wall_bottom_width/2]){
			 cylinder(h=wall_bottom_width+0.1,d2=magnet_d+tolerance1*1.5+0.1,d1=magnet_d+tolerance1*1.5+0.1+wall_bottom_width+0.1,center=true);			 
		    }
		    // magnet right
		    translate([
				   magnetr_x,
				   keypad_size_y/4*3,
				   magnet_z]){
			 magnet(magnet_d+tolerance1*1.5+0.1);
		    }

		    //magnetr_x=keypad_size_x+magnet_d/2+screw_savespace;
		    translate([
				   magnetr_x,
				   keypad_size_y/4*3,
				   magnet_z-magnet_h+wall_bottom_width/2]){
			 cylinder(h=wall_bottom_width+0.1,d2=magnet_d+tolerance1*1.5+0.1,d1=magnet_d+tolerance1*1.5+0.1+wall_bottom_width+0.1,center=true);			 
		    }
		    //            screw right bottom (pcb: right top)
		    translate([
				   screw_top_right_x,
				   screw_top_right_y,
				   magnet_z-magnet_h+wall_bottom_width/2]){
			 cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);
		    }
		    //  	       screw back left bottom (pcb: left bottom)
		    translate([
				   screw_bottom_left_x,
				   screw_bottom_left_y,
				   magnet_z-magnet_h+wall_bottom_width/2]){
			 cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);
		    }
		    //            screw back left bottom (pcb: left top)
		    translate([
				   screw_top_left_x,
				   screw_top_left_y,
			           magnet_z-magnet_h+wall_bottom_width/2]){
			 cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);			 
		    }
		    //            screw back right bottom (pcb: left top)
		    translate([
				   screw_bottom_right_x,
				   screw_bottom_right_y,
			           magnet_z-magnet_h+wall_bottom_width/2]){
			 cylinder(h=wall_bottom_width+0.1,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall_bottom_width+0.1,center=true);			 
		    }
		  
	       }	       
	       
//            top back	    
	       translate([magnetl_x-magnet_d/2-wall-tolerance1,
			  keypad_size_y+tolerance1*2,
			  magnet_z-magnet_h+wall_bottom_width-tolerance2]){
                    cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2,
			  wall,
			  -magnet_z+magnet_h+wall_bottom_width-wall-pcb_width-(onoff_lever_offset_z+onoff_size_lever_z)]);
	       }
/* //            top back left corner */
/* 	       translate([magnetl_x-magnet_d/2-wall-tolerance1, */
/* 			  keypad_size_y+tolerance2*0, */
/* 			  magnet_z-magnet_h+wall_bottom_width-tolerance2]){ */
/*                     cube([wall, */
/* 			  wall, */
/* 			  -magnet_z+magnet_h+wall_bottom_width-wall-pcb_width-(onoff_lever_offset_z+onoff_size_lever_z)]); */
/* 	       } */
/* //            top back right corner */
/* 	       translate([magnetr_x+magnet_d/2+tolerance1, */
/* 			  keypad_size_y+tolerance2*0, */
/* 			  magnet_z-magnet_h+wall_bottom_width-tolerance2]){ */
/*                     cube([wall, */
/* 			  wall, */
/* 			  -magnet_z+magnet_h+wall_bottom_width-wall-pcb_width-(onoff_lever_offset_z+onoff_size_lever_z)]); */
/* 	       } */

//            top back left	    
	       translate([magnetl_x-magnet_d/2-wall-tolerance1,
			  keypad_size_y+tolerance1*2,
			  magnet_z-magnet_h+wall_bottom_width+onoff_lever_offset_z-tolerance2]){
                    cube([-(magnetl_x-magnet_d/2-wall)-(onoff_size_length+2)+(onoff_size_length-4-5)/2+tolerance2,
			  wall,
			  onoff_size_lever_z]);
	       }	    
/* //            top back left corner	     */
/* 	       translate([magnetl_x-magnet_d/2-wall-tolerance1, */
/* 			  keypad_size_y+tolerance2*0, */
/* 			  magnet_z-magnet_h+wall_bottom_width+onoff_lever_offset_z-tolerance2]){ */
/*                     cube([wall, */
/* 			  wall, */
/* 			  onoff_size_lever_z]); */
/* 	       }	     */
//            top back right
	       lever_dist_x=(onoff_size_length+2)/2-(onoff_size_length-4-5)/2;
	       translate([-lever_dist_x+tolerance2,
			  keypad_size_y+tolerance1*2,
			  magnet_z-magnet_h+wall_bottom_width+onoff_lever_offset_z-tolerance2]){
                    cube([magnetr_x+magnet_d/2+wall+lever_dist_x+tolerance2,
			  wall,
			  onoff_size_lever_z]);
	       }	    
/* //            top back right corner */
/* 	       lever_dist_x=(onoff_size_length+2)/2-(onoff_size_length-4-5)/2; */
/* 	       translate([magnetr_x+magnet_d/2+tolerance1, */
/* 			  keypad_size_y, */
/* 			  magnet_z-magnet_h+wall_bottom_width+onoff_lever_offset_z-tolerance2]){ */
/*                     cube([wall, */
/* 			  wall, */
/* 			  onoff_size_lever_z]); */
/* 	       }	     */
//            bottom back left
	       translate([magnetl_x-magnet_d/2-wall-tolerance1,
			  -wall-tolerance1,
			  -pcb_width-pin_block_width-1-lolind32_usb_h-tolerance2]){       
			  //-pcb_width-pin_block_width-1-lolind32_usb_h-tolerance2]){       
		    cube([usb_cutout_left_x-(magnetl_x-magnet_d/2-wall-tolerance1)-tolerance1,
			  wall,
			  lolind32_usb_h]);
	       }
/* //            bottom back left corner	     */
/* 	       translate([magnetl_x-magnet_d/2-wall-tolerance1, */
/* 			  -wall-tolerance2, */
/* 			  -pcb_width-pin_block_width-1-lolind32_usb_h-tolerance2]){        */
/*                     cube([wall, */
/* 			  wall+tolerance2, */
/* 			  lolind32_usb_h]); */
/* 	       } */
	    
//            bottom back right
	       translate([(lolind32_offset_x+2+lolind32_usb_w)+tolerance2,
			  -wall-tolerance1,
			  -pcb_width-pin_block_width-1-lolind32_usb_h-tolerance2]){       
                    cube([(magnetr_x+magnet_d/2+wall)-(lolind32_offset_x+2+lolind32_usb_w)-tolerance2+tolerance1,
			  wall,
			  lolind32_usb_h]);
	       }
/* //            bottom back right corner	     */
/* 	       translate([magnetr_x+magnet_d/2+tolerance1, */
/* 			  -wall-tolerance2, */
/* 			  -pcb_width-pin_block_width-1-lolind32_usb_h-tolerance2]){        */
/*                     cube([wall, */
/* 			  wall+tolerance2, */
/* 			  lolind32_usb_h]); */
/* 	       } */
//            bottom back back
	       translate([magnetl_x-magnet_d/2-wall-tolerance1,
			  -wall-tolerance1,
			  magnet_z-magnet_h+wall_bottom_width-tolerance2]){
                    cube([magnetr_x-magnetl_x+magnet_d+2*wall+tolerance1*2,
			  wall,
			  (-magnet_z+magnet_h+wall_bottom_width-wall)-(pcb_width+pin_block_width+1)-lolind32_usb_h]);
	       }
/* //            bottom back back left corner  */
/* 	       translate([magnetl_x-magnet_d/2-wall-tolerance1, */
/* 			  -wall-tolerance2, */
/* 			  magnet_z-magnet_h+wall_bottom_width-tolerance2]){ */
/*                     cube([wall, */
/* 			  wall+tolerance2, */
/* 			  (-magnet_z+magnet_h+wall_bottom_width-wall)-(pcb_width+pin_block_width+1)-lolind32_usb_h]); */
/* 	       } */
/* //            bottom back back right corner  */
/* 	       translate([magnetr_x+magnet_d/2+tolerance1, */
/* 			  -wall-tolerance2, */
/* 			  magnet_z-magnet_h+wall_bottom_width-tolerance2]){ */
/*                     cube([wall, */
/* 			  wall+tolerance2, */
/* 			  (-magnet_z+magnet_h+wall_bottom_width-wall)-(pcb_width+pin_block_width+1)-lolind32_usb_h]); */
/* 	       } */
//            left
	       translate([magnetl_x-magnet_d/2-wall-tolerance1,
			  -tolerance1,
			  magnet_z-magnet_h+wall_bottom_width]){
		    cube([wall,
			  keypad_size_y+tolerance1*3,
			  case_back_wall_height]);			  
		          //-magnet_z+magnet_h+wall_bottom_width+keypad_size_z-wall-wall-tolerance1]);
	       }
//            right
	       translate([magnetr_x+magnet_d/2+tolerance1,
			  -tolerance1,
			  magnet_z-magnet_h+wall_bottom_width]){
		    cube([wall,
			  keypad_size_y+tolerance1*3,
			   case_back_wall_height]);
			  //-magnet_z+magnet_h+wall_bottom_width-pcb_width-pin_block_width-lolind32_usb_h-tolerance1]);
			  //-magnet_z+magnet_h+wall_bottom_width+keypad_size_z-wall-wall-tolerance1]);
	       }
	       }
//            keypad middle support
	       translate([keypad_size_x/2+3/4,
			  0,
			  magnet_z-magnet_h+wall_bottom_width]){
		    cube([wall*3/4,
			  5+0*keypad_size_y,
			  -magnet_z+magnet_h-wall_bottom_width-pcb_width]);
	       }
//            keypad middle support back
	       translate([keypad_size_x/2+3/4,
			  -tolerance1*2,
			  magnet_z-magnet_h+wall_bottom_width]){
		    cube([wall*3/4,
			  keypad_size_y+tolerance1*4,
			  case_back_wall_height]);
	       }
//            screw front left back (pcb: left top)
	       difference(){   
		    union(){
			 translate([
					screw_top_left_x, //lolind32_offset_x-(screw_bottom_d+4)/2-1-screw_left_reality_offset_x,
					screw_top_left_y, //onoffpcb_y-(screw_bottom_d+4)/2,
					(-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){
			      cylinder(h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width),d=(screw_bottom_d+4),center=true);
			      // screw front left bottom support
			      screw_front_left_bottom_support_magnet_move_x=1;
			      //screw_top_support_length=screw_bottom_d+screw_front_left_bottom_support_magnet_move_x*2;
			      screw_top_support_length=screw_top_left_x-(magnetl_x-magnet_d/2-wall-tolerance1);
			      translate([
					     -screw_top_support_length,
					     -(screw_bottom_d+4)/2,
					     -(-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2
					     +magnet_z-magnet_h+wall_bottom_width]){
				   cube([
					  screw_top_support_length,
					  (screw_bottom_d+4),
					  case_back_wall_height],
					 //-pcb_width-(magnet_z-magnet_h+wall_bottom_width)],
				   center=false);
			      }

			 }
		    }
		    translate([
				   screw_top_left_x, //lolind32_offset_x-(screw_bottom_d+4)/2-1-screw_left_reality_offset_x,
				   screw_top_left_y, //onoffpcb_y-(screw_bottom_d+4)/2,
				   -screw_bottom_height/2-pcb_width]){
			 cylinder(h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width)+1,d=screw_d,center=true);
			 translate([0,0,-screw_k-pcb_cone_distance]){cylinder(h=screw_bottom_height+0.1,d=screw_dk+tolerance2,center=true);}
			 translate([0,0,screw_bottom_height/2-screw_k/2-pcb_cone_distance]){cylinder(h=screw_k,d1=screw_dk+tolerance2,d2=3,center=true);}
		    }
	       }
//  	       screw bottom left back
	       difference(){
		    screw_back_bottom_top_h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width);
		    screw_back_bottom_back_h=-(magnet_z-magnet_h+wall_bottom_width)-(pcb_width+pin_block_width+1+lolind32_usb_h*0)-tolerance2;
		    union(){
			 translate([
					screw_bottom_left_x,
					screw_bottom_left_y,
					(-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){			      
			      cylinder(h=screw_back_bottom_back_h*0+screw_back_bottom_top_h,d=(screw_bottom_d+4-tolerance1*0),center=true);
			 }
			 screw_bottom_left_support_width=(screw_bottom_d+4-tolerance1)*3/4;
			 translate([
					screw_bottom_left_x, 
					screw_bottom_left_y-screw_bottom_left_support_width/2, //(screw_bottom_d+4)/2-wall/2-(screw_bottom_d+4-tolerance1*0)/4,
					(magnet_z-magnet_h+wall_bottom_width)+screw_back_bottom_back_h/2]){
			      cube([(screw_bottom_d+4-tolerance1*0),screw_bottom_left_support_width,screw_back_bottom_back_h],center=true);
			 }
		    }
		    translate([
				   screw_bottom_left_x, //lolind32_offset_x-(screw_bottom_d+4)/2-1-screw_left_reality_offset_x,
				   screw_bottom_left_y, //(screw_bottom_d+4)/2-wall/2,
				   //(-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){
				   -screw_bottom_height/2-pcb_width]){
			 cylinder(h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width)+1,d=screw_d,center=true);
			 translate([0,0,-screw_k-pcb_cone_distance]){cylinder(h=screw_bottom_height+0.1,d=screw_dk+tolerance2,center=true);}
			 translate([0,0,screw_bottom_height/2-screw_k/2-pcb_cone_distance]){cylinder(h=screw_k,d1=screw_dk+tolerance2,d2=3,center=true);}
		    }
		    // screw back left bottom tolerance to bottom
		    screw_back_left_bottom_tolerance_to_bottom_z=-pcb_width-(magnet_z-magnet_h+wall_bottom_width)-screw_back_bottom_back_h;
		    screw_back_left_bottom_tolerance_to_bottom_y=(screw_bottom_d+4-tolerance1*0)/2-(screw_bottom_d+4-tolerance1*2)/2;
		    overlap=1;
		    translate([
		    		   screw_bottom_left_x, 
		    		   ((screw_bottom_d+4)/2-wall/2)-(screw_bottom_d+4-tolerance1*0)/2-screw_back_left_bottom_tolerance_to_bottom_y/2,
		    		   (magnet_z-magnet_h+wall_bottom_width)+screw_back_bottom_back_h+screw_back_left_bottom_tolerance_to_bottom_z/2+overlap/2]){
		    	 cube([(screw_bottom_d+4-tolerance1*0),screw_back_left_bottom_tolerance_to_bottom_y,screw_back_left_bottom_tolerance_to_bottom_z+overlap],center=true);
		    }
	       }

//  	       screw bottom right back
	       difference(){
		    screw_back_bottom_top_h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width);
		    screw_back_bottom_back_h=-(magnet_z-magnet_h+wall_bottom_width)-(pcb_width+pin_block_width+1+lolind32_usb_h*0)-tolerance2;
		    union(){
			 translate([
					screw_bottom_right_x,
					screw_bottom_right_y,
					(-pcb_width+(magnet_z-magnet_h+wall_bottom_width))/2]){			      
			      cylinder(h=screw_back_bottom_back_h*0+screw_back_bottom_top_h,d=(screw_bottom_d+4-tolerance1*0),center=true);
			 }
			 screw_bottom_left_support_width=(screw_bottom_d+4-tolerance1)*3/4;
			 translate([
					screw_bottom_right_x, 
					screw_bottom_left_y-screw_bottom_left_support_width/2, //(screw_bottom_d+4)/2-wall/2-(screw_bottom_d+4-tolerance1*0)/4,
					(magnet_z-magnet_h+wall_bottom_width)+screw_back_bottom_back_h/2]){
			      cube([(screw_bottom_d+4),screw_bottom_left_support_width,screw_back_bottom_back_h],center=true);
			 }
			 translate([
					screw_bottom_right_x+((screw_bottom_d+4)/2+tolerance1)/2,
					screw_bottom_left_y+(screw_bottom_d+4)/2*0,
					(magnet_z-magnet_h+wall_bottom_width)+screw_back_bottom_back_h/2]){
			      cube([(screw_bottom_d+4)/2+tolerance1,screw_bottom_d+4,screw_back_bottom_back_h],center=true);
			 }
			 
		    }
		    translate([
				   screw_bottom_right_x,
				   screw_bottom_right_y,
				   -screw_bottom_height/2-pcb_width]){
			 cylinder(h=-pcb_width-(magnet_z-magnet_h+wall_bottom_width)+1,d=screw_d,center=true);
			 translate([0,0,-screw_k-pcb_cone_distance]){cylinder(h=screw_bottom_height+0.1,d=screw_dk+tolerance2,center=true);}
			 translate([0,0,screw_bottom_height/2-screw_k/2-pcb_cone_distance]){cylinder(h=screw_k,d1=screw_dk+tolerance2,d2=3,center=true);}
		    }
		    // screw bottom right back tolerance to bottom
		    screw_back_left_bottom_tolerance_to_bottom_z=-pcb_width-(magnet_z-magnet_h+wall_bottom_width)-screw_back_bottom_back_h;
		    screw_back_left_bottom_tolerance_to_bottom_y=(screw_bottom_d+4-tolerance1*0)/2-(screw_bottom_d+4-tolerance1*2)/2;
		    overlap=1;
		    translate([
		    		   screw_bottom_right_x, 
		    		   ((screw_bottom_d+4)/2-wall/2)-(screw_bottom_d+4-tolerance1*0)/2-screw_back_left_bottom_tolerance_to_bottom_y/2,
		    		   (magnet_z-magnet_h+wall_bottom_width)+screw_back_bottom_back_h+screw_back_left_bottom_tolerance_to_bottom_z/2+overlap/2]){
		    	 cube([(screw_bottom_d+4-tolerance1*0),screw_back_left_bottom_tolerance_to_bottom_y,screw_back_left_bottom_tolerance_to_bottom_z+overlap],center=true);
		    }
	       }
	       
               // screw right (pcb: right top)
	       support_height=case_back_wall_height;
	       translate([
 	       		      screw_top_right_x,
	       		      screw_top_right_y,
	       		      -screw_bottom_height/2-pcb_width]){
	       	    difference(){
			 union(){
			      cylinder(h=screw_bottom_height,d=(screw_bottom_d+4)+0.01,center=true);
			      translate([
					     0,
					     ((screw_bottom_d+4)-3)/2+tolerance1/2,
					     -screw_bottom_height/2+support_height/2]){
				   cube([screw_bottom_d+4,((screw_bottom_d+4)-3)+tolerance1,support_height],center=true);
			      }
			 }
			      // screw right bottom support
			 cylinder(h=screw_bottom_height+1,d=screw_d,center=true);
			 translate([0,0,-screw_k-pcb_cone_distance]){cylinder(h=screw_bottom_height+0.1,d=screw_dk+tolerance2,center=true);}
			 translate([0,0,screw_bottom_height/2-screw_k/2-pcb_cone_distance]){cylinder(h=screw_k,d1=screw_dk+tolerance2,d2=3,center=true);}
		    }
	       }
	       
	  }
     }
}


module case_front(){
     intersection(){
	  case_outer();
	  front(walls=false);
     }

     // front keypad frame
     translate([-tolerance1,
		-tolerance1,
		keypad_size_z-wall-3]){
	  difference(){
	       minkowski(){
		    cube([keypad_size_x+tolerance1*2,
			  keypad_size_y+tolerance1*3,
			  wall+3-1]);
		    //cylinder(r=wall-tolerance1/2+0.042,h=1);
		    cylinder(r=wall+0*(-tolerance2+0.042*3),h=1);
	       }
	       overlap=1;
	       translate([0,0,-overlap]){
		    cube([keypad_size_x+tolerance1*2,keypad_size_y+tolerance1*3,wall+3+1+overlap*2]);
		    remove_screw_hole=wall+0.1;
		    translate([0,0,-overlap-wall]){
		    cube([keypad_size_x+tolerance1*2+remove_screw_hole,keypad_size_y+tolerance1*3,wall+3+1+overlap*2]);
		    }
	       }
	  }
     }
     
     
     difference(){
     	  case_outer();
     	  case_inner();
     	  translate([0,0,-front_outer_height]){
     	       case_outer(crad=0);
     	  }
     }
}

module case_back(){
     translate([0,0,0]){
	  difference(){
	       case_outer();
	       difference(){
	       	    case_outer();
	       	    back(walls=false);
	       	    difference(){
			 case_outer();
	       		 case_inner();
	       		 back(walls=false);
	       		 translate([0,0,case_back_wall_height+wall_bottom_width]){
	       		      case_outer(crad=0);
	       		 }
	       	    }
	       }

	  }
     }
}

module lipo_protector_print(){
     rotate([0,-90,0]){
	  lipo_protector();
     }
}

module front_print(){
     translate([-60,0,keypad_size_z]){
	  rotate([0,180,0]){
	       case_front();
	       *front(walls=true);
	  }
     }
     *case_front();
     *front();

}

module back_print(){
     translate([90,0,-magnet_z+magnet_h]){
	  case_back();
	  *back();
     }
     *case_back();
     *back(walls=true);
}

front_print();
lipo_protector_print();
back_print();
