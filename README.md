# oskar_concertina_case
Case for Oskar Concertina.
Oskar Concertina is a open-source, mobile braillekeyboard.
The arrangement of 8 keys in a braille cell block allows Oskar to be controlled without a supporting surface.

# Copyright
Copyright 2019 Johannes Strelka-Petz, johannes_at_strelka.at

# License
This documentation describes Open Hardware and is licensed under the
CERN Open Hardware Licence v1.2 or Later

You may redistribute and modify this documentation under the terms of the
CERN OHL v.1.2. (http://ohwr.org/cernohl).
This documentation is distributed
WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE.
Please   see   the   CERN   OHL   v.1.2  for   applicable conditions

# Build
[openscad](https://www.openscad.org) concertina.scad
find front_print(), lipo_protector_print(), back_print() near the end of concertina.scad and disable or enable output with [diable modifier](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Modifier_Characters#Disable_Modifier)

# Print
front.stl (~2 hours), back.stl (~1.5 hours), lipo_protector.stl (~15 minuts)

# Contact
[https://oskar.ddns.mobi](https://oskar.ddns.mobi)
Johannes Strelka-Petz <johannes_at_oskar.ddns.mobi>